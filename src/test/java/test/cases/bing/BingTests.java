package test.cases.bing;

import pages.bing.BingHomePage;
import pages.bing.BingResultsPage;

import org.junit.Test;

public class BingTests extends BaseTest {

    @Test
    public void searchInputVisible_when_homePageNavigated() {
        BingHomePage home = new BingHomePage(actions.getDriver());

        home.navigateToPage();

        home.assertSearchInputVisible();
    }

    @Test
    public void searchResultsVisible_when_termIsSearched() {
        BingHomePage home = new BingHomePage(actions.getDriver());
        home.navigateToPage();

        home.searchAndSubmit("Telerik Academy Alpha");

        BingResultsPage results = new BingResultsPage(actions.getDriver());
        results.assertResultIsPresent("Telerik Academy Alpha");
    }
}
