package test.cases.jira;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;


import java.time.Duration;

import static utils.Constants.*;

public class BaseTest {

    protected WebDriver driver;
    protected WebDriverWait wait;


    @Before
    public void testInit() {
        //setup browser
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        driver.manage().window().maximize();
    }


    public void login() {
        testInit();
        //enter email
        driver.get(PROJECT_PAGE);
        WebElement username = driver.findElement(By.xpath(USERNAME_XPATH));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(USERNAME_XPATH)));
        username.sendKeys(EMAIL);
        driver.findElement(By.xpath(CONTINUE_BUTTON_XPATH)).submit();

        //enter password
        WebElement password = driver.findElement(By.xpath(PASSWORD_XPATH));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PASSWORD_XPATH)));
        password.sendKeys(PASSWORD);
        driver.findElement(By.xpath(LOGIN_BUTTON_XPATH)).submit();

    }


    @After
    public void tearDown() {
        driver.quit();
    }
}
