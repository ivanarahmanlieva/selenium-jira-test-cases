package test.cases.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import static utils.Constants.*;


public class JiraTests extends BaseTest {


    @Test
    public void createStory() throws InterruptedException {
        testInit();
        driver.get(PROJECT_PAGE);
        login();

        Thread.sleep(8000);
        wait.until(ExpectedConditions.urlToBe(PROJECT_PAGE));

        WebElement createButton = driver.findElement(By.className(CREATE_MAIN_BUTTON_CLASS_NAME));
        wait.until(ExpectedConditions.elementToBeClickable(By.className(CREATE_MAIN_BUTTON_CLASS_NAME)));
        createButton.click();

        WebElement changeType = driver.findElement(By.xpath(CHANGE_TYPE_XPATH));
        changeType.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(STORY_TYPE_XPATH)));
        WebElement storyType = driver.findElement(By.xpath(STORY_TYPE_XPATH));
        storyType.click();


        wait.until(ExpectedConditions.elementToBeClickable(By.id(SUMMARY_ID)));
        WebElement summaryStory = driver.findElement(By.id(SUMMARY_ID));
        summaryStory.sendKeys(SUMMARY_STORY_TEXT);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(DESCRIPTION_XPATH)));
        WebElement descriptionStory = driver.findElement(By.xpath(DESCRIPTION_XPATH));
        descriptionStory.sendKeys(DESCRIPTION_TEXT);


        WebElement createStoryButton = driver.findElement(By.cssSelector(CREATE_STORY_BUG_BUTTON_CSS));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(CREATE_STORY_BUG_BUTTON_CSS)));
        createStoryButton.click();

    }

    @Test
    public void createBug() throws InterruptedException {
        testInit();
        driver.get(PROJECT_PAGE);
        login();

        Thread.sleep(8000);
        wait.until(ExpectedConditions.urlToBe(PROJECT_PAGE));

        WebElement createButton = driver.findElement(By.className(CREATE_MAIN_BUTTON_CLASS_NAME));
        wait.until(ExpectedConditions.elementToBeClickable(By.className(CREATE_MAIN_BUTTON_CLASS_NAME)));
        createButton.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id(CHANGE_ISSUETYPE_DROPDOWN_ID)));
        WebElement changeIssueTypeDropDown = driver.findElement(By.id(CHANGE_ISSUETYPE_DROPDOWN_ID));
        changeIssueTypeDropDown.click();
        Actions actionBug = new Actions(driver);
        actionBug.sendKeys("Bug");
        actionBug.sendKeys(Keys.ENTER);
        actionBug.perform();

        wait.until(ExpectedConditions.elementToBeClickable(By.id(SUMMARY_ID)));
        WebElement summaryBug = driver.findElement(By.id(SUMMARY_ID));
        summaryBug.sendKeys(SUMMARY_BUG_TEXT);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(DESCRIPTION_XPATH)));
        WebElement descriptionBug = driver.findElement(By.xpath(DESCRIPTION_XPATH));
        descriptionBug.sendKeys(DESCRIPTION_TEXT);

        //setting priority
        WebElement showPriority = driver.findElement(By.xpath(SHOW_PRIORITY_XPATH));
        showPriority.click();
        WebElement selectPriority = driver.findElement(By.xpath(SELECT_PRIORITY_XPATH));
        selectPriority.click();

        WebElement createBugButton = driver.findElement(By.cssSelector(CREATE_STORY_BUG_BUTTON_CSS));
        createBugButton.click();

    }

    @Test
    public void link_Story_Bug() throws InterruptedException {
        testInit();
        driver.get(PROJECT_PAGE);
        login();

        Thread.sleep(8000);
        wait.until(ExpectedConditions.urlToBe(PROJECT_PAGE));
        driver.navigate().to(STORY_LINK);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(LINK_BUTTON_XPATH)));
        WebElement linkButton = driver.findElement(By.xpath(LINK_BUTTON_XPATH));
        linkButton.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ISSUE_SEARCH_BOX_XPATH)));
        WebElement issueSearchBox = driver.findElement(By.xpath(ISSUE_SEARCH_BOX_XPATH));
        issueSearchBox.click();
        Actions actions = new Actions(driver);
        actions.sendKeys("SJP-20");
        actions.sendKeys(Keys.ENTER);
        actions.perform();


        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(LINK_FINAL_BUTTON_XPATH)));
        WebElement linkFinalButton = driver.findElement(By.xpath(LINK_FINAL_BUTTON_XPATH));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(LINK_FINAL_BUTTON_XPATH)));
        linkFinalButton.click();
    }

    @Test
    public void authenticateUser() throws InterruptedException {
        testInit();
        login();

        Thread.sleep(8000);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ACCOUNT_XPATH)));
        Assert.assertEquals(driver.getCurrentUrl(),
                PROJECT_PAGE);
    }
}