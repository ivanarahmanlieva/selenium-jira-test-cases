package utils;

public class Constants {

    public static final String PROJECT_PAGE = "https://telerika-a38-team4.atlassian.net/jira/software/projects/SJP/boards/7";
    public static final String CREATE_MAIN_BUTTON_CLASS_NAME = "css-cxhge5";

    public static final String SUMMARY_ID = "summary-field";
    public static final String SUMMARY_STORY_TEXT = "Story is created with Selenium";
    public static final String SUMMARY_BUG_TEXT = "Bug is created with Selenium";

    public static final String DESCRIPTION_XPATH = "/html/body/div[15]/div[3]/div/div[3]/div/div/" +
            "section/div[2]/div/div/div/div/form/div[2]/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]";
    public static final String DESCRIPTION_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
            "Aliquam sit amet augue urna. Ut eu finibus quam. Vestibulum mattis lacinia felis.";

    public static final String CREATE_STORY_BUG_BUTTON_CSS = "#jira > div.atlaskit-portal-container > " +
            "div:nth-child(3) > div > div:nth-child(3) > div > div > section > div.css-1t4cmpd > button > span";

    public static final String USERNAME_XPATH = "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[1]" +
            "/div[1]/div/div/div/input";
    public static final String EMAIL = "ivana.rahmanlieva.a38@learn.telerikacademy.com";
    public static final String CONTINUE_BUTTON_XPATH = "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div" +
            "[2]/form/button/span/span";
    public static final String PASSWORD_XPATH = "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div" +
            "[2]/form/div[1]/div[2]/div/div/div/div/div/input";
    public static final String PASSWORD = "Telerik2021*";
    public static final String LOGIN_BUTTON_XPATH = "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/button/span/span";

    public static final String CHANGE_ISSUETYPE_DROPDOWN_ID = "issue-create.ui.modal.create-form.type-picker.issue-type-select";

    public static final String STORY_LINK = "https://telerika-a38-team4.atlassian.net/browse/SJP-28";
    public static final String CHANGE_TYPE_XPATH = "//div[@id='issue-create.ui.modal.create-form.type-picker.issue-type-select']";
    public static final String STORY_TYPE_XPATH = "//div[contains(text(), 'Story')]";

    public static final String SHOW_PRIORITY_XPATH = "//*[@id='priority-container']/div/div[1]/div/div/div[1]/div[1]/div/div[2]/div";
    public static final String SELECT_PRIORITY_XPATH = "//div[contains(text(), 'High')]";

    public static final String LINK_BUTTON_XPATH = "//*[@id='ak-main-content']/div/div/div[2]/div[2]/div[2]/div[1]" +
            "/div/div[1]/div/div[4]/span/div/button";
    public static final String ISSUE_SEARCH_BOX_XPATH = "/html/body/div[5]/div[1]/div[2]/div[3]/div[2]" +
            "/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[3]/div[2]/div/div[2]/div[1]/div[2]/div/div";
    public static final String LINK_FINAL_BUTTON_XPATH = "//*[@id='ak-main-content']/div/div/div[2]/" +
            "div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[3]/div[2]/div/div[2]/div[2]/div[2]/div[1]/div/button/span";

    public static final String ACCOUNT_XPATH = "/html/body/div[1]/div[1]/div[2]/div[1]/div/header/div/span[4]/span/div/div/button";

}
