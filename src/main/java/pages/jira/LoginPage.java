package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage extends BaseJiraPage {
    private WebDriver driver;
    private static WebDriverWait wait;


    //elements of login page
    public LoginPage(WebDriver webDriver) {
        super(webDriver, "jira.url");
        driver = webDriver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public WebElement getUser() {
        return driver.findElement(By.id("username"));
    }

    public WebElement getPassword() {
        return driver.findElement(By.id("password"));
    }

    public WebElement getLoginButton() {
        return driver.findElement(By.cssSelector("#login-submit > span"));
    }


    public void getUser(String userKey) {
        String username = Utils.getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.loginButton");

        actions.waitFor(2000);

        actions.waitForElementVisible("jira.loginPage.password");
        actions.waitForElementVisible("jira.loginPage.continueButton");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.loginButton");
    }

    public void authenticateUser(String username, String password) {
        wait.until(ExpectedConditions.elementToBeClickable(getLoginButton()));
        getUser().sendKeys(username);
        getLoginButton().click();

        wait.until(ExpectedConditions.elementToBeClickable(getPassword()));

        getPassword().sendKeys(password);
        getLoginButton().click();

    }

}

