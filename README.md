**Instructions**
1. In order for the tests to run - change credentials, url for project, story link and bug key
2. All constants are in src/main/java/utils/Constants
3. All tests are in src/test/java/test.cases/jira/JiraTests
4. Story and bug descriptions - Lorem Ipsum used as test text
5. Try running tests one by one if there is a problem with their passing